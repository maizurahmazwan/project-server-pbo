package models

type (
	Copywriting struct {
		ID     string
		Msg    string
		Status string
		Title  string
	}

	RefreshResponse struct {
		Code    string      `json:"code"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
)
