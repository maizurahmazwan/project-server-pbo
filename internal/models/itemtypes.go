package models

import "time"

type (
	ItemTypes struct {
		SID       int       `json:"sid"`
		Name      string    `json:"name"`
		Value     int       `json:"value"`
		CreatedAt time.Time `json:"created_at" db:"created_at"`
		UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
	}
)
