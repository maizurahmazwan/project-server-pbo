package models

import "github.com/golang-jwt/jwt"

type (
	JWTCustomClaims struct {
		Name              string `json:"name"`
		JWTStandardClaims jwt.StandardClaims
	}

	LoginResponse struct {
		Token string             `json:"token"`
		User  PocketBaseUserInfo `json:"user"`
	}

	PocketBaseProfile struct {
		/* "profile": {
			"@collectionId": "d2972397d45614e",
			"@collectionName": "profiles",
			"avatar": null,
			"created": "2022-06-24 06:24:18.435",
			"id": "e22581b6f1d44ea",
			"name": "",
			"updated": "2022-06-24 06:24:18.435",
			"userId": "8171022dc95a4ed"
		  } */
		CollectionId string  `json:"@collectionId"`
		Avatar       *string `json:"avatar"`
		Id           string  `json:"id"`
		Name         string  `json:"name"`
	}

	PocketBaseUserInfo struct {
		ID                     string            `json:"id"`
		Created                string            `json:"created"`
		Updated                string            `json:"updated"`
		Email                  string            `json:"email"`
		LastResetSentAt        string            `json:"lastResetSentAt"`
		Verified               bool              `json:"verified"`
		LastVerificationSentAt string            `json:"lastVerificationSentAt"`
		Profile                PocketBaseProfile `json:"profile"`
	}

	ProfileInfo struct {
		CollectionID string `json:"collectionId"`
	}
)
