package models

import (
	"fmt"
	"time"
)

type (
	DialingCodes struct {
		Sid         int       `json:"sid" db:"sid"`
		Code        string    `json:"code" db:"code"`
		Country     string    `json:"country" db:"country"`
		CallingCode string    `json:"calling_code" db:"calling_code"`
		CreatedAt   time.Time `json:"created_at" db:"created_at"`
		UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
	}
)

func (d DialingCodes) String() string {
	return fmt.Sprintf("[%s,%s,%s]", d.Code, d.Country, d.CallingCode)
}
