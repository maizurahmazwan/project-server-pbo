package handlers

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func Postcodes(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "postcodes", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func UploadPostcodes(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	if c.Request().Method == "POST" {
		file, _, err := c.Request().FormFile("csvFile")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting uploaded file")
			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "postcodes", templateDataMap)

		}

		// validate the uploaded file
		reader := csv.NewReader(file)
		reader.Comma = ','
		reader.Comment = '#'
		reader.LazyQuotes = true
		recordsFromCSV, err := reader.ReadAll()
		if len(recordsFromCSV) == 0 || err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error opening uploaded csv file")

			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "postcodes", templateDataMap)

		}

		// remove first row if it's header
		if strings.EqualFold(recordsFromCSV[0][1], "postcode_from") && strings.EqualFold(recordsFromCSV[0][2], "postcode_to") {
			recordsFromCSV = recordsFromCSV[1:]
		}

		// // temporarily store postcode range for checking
		// m := make(map[int]int)

		// sanity check on each column
		for _, record := range recordsFromCSV {
			data := models.Postcodes{
				Country:      record[0],
				PostcodeFrom: record[1],
				PostcodeTo:   record[2],
				CityName:     record[3],
				StateName:    record[4],
				StateCode:    record[5],
			}

			// make sure all fields are not empty
			if data.Country == "" ||
				data.PostcodeFrom == "" ||
				data.PostcodeTo == "" ||
				data.CityName == "" ||
				data.StateName == "" ||
				data.StateCode == "" {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in csv content")

				DataMap["UploadError"] = true
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "postcodes", templateDataMap)
			}

			// // check for overlap in postcode range
			// from, _ := strconv.Atoi(data.PostcodeFrom)
			// to, _ := strconv.Atoi(data.PostcodeTo)
			// for key, value := range m {
			// 	if from >= key && to <= value {
			// 		loggerWithTrace.Error().Err(err).Caller().Msg("error in csv content")

			// 		DataMap["UploadError"] = true
			// 		DataMap["Message"] = fmt.Sprintf("duplicate postcode range detected - [%d,%d] and [%d,%d]", key, value, from, to)
			// 		templateDataMap["DataMap"] = DataMap

			// 		return c.Render(http.StatusOK, "postcodes", templateDataMap)
			// 	}
			// }
			// m[from] = to
		}

		// load all rows from database
		var postcodesFromDB []models.Postcodes

		errDB := database.WrapSelect(dbconnections.MDMDBPool, ctx, &postcodesFromDB, "GetAllFromCities")
		if errDB != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapSelect error")
			DataMap["UploadError"] = true
			DataMap["Message"] = errDB
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "postcodes", templateDataMap)
		}

		// insert - find additional row inside csv
		var insertFromCSV []models.Postcodes

		for _, record := range recordsFromCSV {
			data := models.Postcodes{
				Country:      strings.TrimSpace(record[0]),
				PostcodeFrom: strings.TrimSpace(record[1]),
				PostcodeTo:   strings.TrimSpace(record[2]),
				CityName:     strings.TrimSpace(record[3]),
				StateName:    strings.TrimSpace(record[4]),
				StateCode:    strings.TrimSpace(record[5]),
			}

			if !strings.EqualFold(data.PostcodeFrom, "postcode_from") && !strings.EqualFold(data.PostcodeTo, "postcode_to") {
				insertFromCSV = append(insertFromCSV, data)
			}

		}

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true
		DataMap["InsertCodes"] = insertFromCSV
		DataMap["ExitingRowsCount"] = len(postcodesFromDB)
		DataMap["NewRowsCount"] = len(insertFromCSV)

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "updatepostcodes", templateDataMap)

	}

	// user did not select a file and click submit button.
	return echo.ErrBadRequest
	//return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH")+"/hscodes")
}

func UpdatePostcodes(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		err := c.Request().ParseForm()

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error parsing form")
		}

	}

	if c.Request().Method == "POST" {

		var dataToInsert []models.Postcodes
		var data models.Postcodes

		for formKey, inputValues := range c.Request().Form {
			if formKey == "postcodesValue[]" {

				for _, value := range inputValues {
					row := strings.Split(value, ",")

					data.Country = row[0]
					data.PostcodeFrom = row[1]
					data.PostcodeTo = row[2]
					data.CityName = row[3]
					data.StateName = row[4]
					data.StateCode = row[5]

					dataToInsert = append(dataToInsert, data)
				}
			}
		}

		_, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "DeleteAllFromCities")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in deleting all in cities table")

			DataMap["UploadSuccess"] = false
			DataMap["UploadError"] = true

			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "postcodes", templateDataMap)

		} else {
			// log activity
			utilities.InsertActivity(c, "UpdatePostcodes: DeleteAllFromCities", DataMap["Username"].(string), "cities")

			// proceed with insert after successful delete
			rowsToInsert := [][]interface{}{}

			for i := 0; i < len(dataToInsert); i++ {
				row := []interface{}{
					dataToInsert[i].Country,
					dataToInsert[i].PostcodeFrom,
					dataToInsert[i].PostcodeTo,
					dataToInsert[i].CityName,
					dataToInsert[i].StateName,
					dataToInsert[i].StateCode,
				}
				rowsToInsert = append(rowsToInsert, row)
			}

			copyCount, err := database.WrapCopyFrom(dbconnections.MDMDBPool, ctx, pgx.Identifier{"cities"},
				[]string{"country", "postcode_from", "postcode_to", "city_name", "state_name", "state_code"},
				pgx.CopyFromRows(rowsToInsert))

			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in insert new records to cities table")
				DataMap["UploadSuccess"] = false
				DataMap["UploadError"] = true
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "postcodes", templateDataMap)
			}

			if int(copyCount) != len(dataToInsert) {
				loggerWithTrace.Info().Msgf("Expected CopyFrom to return %d copied rows, but got %d", len(rowsToInsert), copyCount)

				DataMap["UploadSuccess"] = false
				DataMap["UploadError"] = true

				DataMap["Message"] = "The number rows to insert not equal to the number of copied rows."
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "postcodes", templateDataMap)
			} else {
				// log activity
				utilities.InsertActivity(c, "UpdatePostcodes: InsertAllIntoCities", DataMap["Username"].(string), "cities")

				copyCountString := fmt.Sprintf("%d", copyCount)
				DataMap["Message"] = copyCountString + " new records inserted."
				DataMap["UploadSuccess"] = true
				DataMap["UploadError"] = false

				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "postcodes", templateDataMap)
			}

		}

	}

	// user token has expired, return the user back to home page
	return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))

}
