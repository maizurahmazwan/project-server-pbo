package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/http/responses"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

const CookieName = "LoginToken"

var TokenString string

func Login(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}

	//default behaviour is to assume that user submitted
	// wrong username/password
	DataMap["LoginError"] = true
	DataMap["ImageFilename"] = "backoffice-404.jpg" // show error background
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	// verify email and password
	if c.FormValue("password") != "" && c.FormValue("email") != "" {
		email := c.FormValue("email")
		password := c.FormValue("password")

		// Run user authentication with PocketBase
		url := env.Get("POCKET_BASE_AUTH_VIA_EMAIL")

		values := echo.Map{"email": email, "password": password}
		jsonValue, _ := json.Marshal(values)
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting response from pocket base.")
			return c.String(http.StatusInternalServerError, err.Error())
		}

		var pocketBaseResponseBody bytes.Buffer
		_, err = io.Copy(&pocketBaseResponseBody, resp.Body) // io.Copy is safer and faster than ioutil.ReadAll

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting response from pocket base.")
			return c.String(http.StatusInternalServerError, err.Error())
		}

		var login models.LoginResponse
		err = json.Unmarshal(pocketBaseResponseBody.Bytes(), &login)

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting response from pocket base.")
			return c.String(http.StatusInternalServerError, err.Error())
		}

		if login.Token != "" {
			var avatar string = ""
			if login.User.Profile.Avatar != nil &&
				login.User.Profile.CollectionId != "" &&
				login.User.Profile.Id != "" {
				avatar = fmt.Sprintf("%s/%s/%s/%s",
					env.Get("POCKET_BASE_AVATAR"),
					login.User.Profile.CollectionId,
					login.User.Profile.Id,
					*login.User.Profile.Avatar)
			}

			/*if login.User.Profile.Name == "" {
				DataMap["LoginError"] = false
				DataMap["LoginMissingUsername"] = true
				return c.Render(http.StatusOK, "homeisloginpage", DataMap)
			}*/

			DataMap["Username"] = email
			DataMap["Avatar"] = avatar
			DataMap["MDMPath"] = env.Get("MDM_PATH")
			DataMap["LoginError"] = false
			DataMap["LoginMissingUsername"] = true

			jwtExpires, err := strconv.Atoi(env.Get("JWT_EXPIRES_IN"))
			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error getting JWT_EXPIRES_IN environment variable. Setting to default 24 hours.")
				jwtExpires = 24
			}

			claims := jwt.MapClaims{}
			claims["email"] = login.User.Email //embed username and email inside the token string
			claims["username"] = login.User.Profile.Name
			claims["avatar"] = avatar

			// Set JWT expiration time. See https://www.rfc-editor.org/rfc/rfc7519.html#page-9
			claims["exp"] = time.Now().Add(time.Hour * time.Duration(jwtExpires)).Unix()

			// create JWT token with claims
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

			// generate encoded token and send it as response.
			TokenString, err := token.SignedString([]byte(env.Get("JWT_SECRET")))
			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error getting JWT_SECRET environment variable")
				return err
			}

			// create the token cookie for client(browser)
			tokenCookie := http.Cookie{
				Name:     CookieName,
				Value:    TokenString,
				HttpOnly: true, // Http-only helps mitigate the risk of client side script accessing the protected cookie
			}
			c.SetCookie(&tokenCookie)

			// After the cookie is created, the client(browser) will send in the cookie
			// for every request. Our server side program will unpack the tokenString inside the cookie's Value
			// for authentication before serving...
			// This is one big advantage of JWT over session. The burden has been shifted to client instead of taking memory space
			// on the server side. This helps a lot with the scaling process.

			return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH")+"/dashboard")

		}

	}
	return c.Render(http.StatusOK, "homeisloginpage", DataMap)

}
