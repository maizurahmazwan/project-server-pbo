package handlers

import (
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/env"
)

func LogoutProperly(c echo.Context) error {

	// create the token cookie for client(browser)
	tokenCookie := http.Cookie{
		Name:     CookieName,
		Value:    TokenString,
		MaxAge:   -1,
		Expires:  time.Now().Add(-24 * time.Hour),
		HttpOnly: true, // Http-only helps mitigate the risk of client side script accessing the protected cookie
	}
	c.SetCookie(&tokenCookie)

	DataMap := echo.Map{}
	DataMap["Logout"] = true
	DataMap["MDMPath"] = env.Get("MDM_PATH")
	return c.Render(http.StatusOK, "homeisloginpage", DataMap)

}
