package handlers

import (
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strconv"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func DashBoard(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		//	uriSegments := strings.Split(r.URL.Path, "/")

		// extract data from activities with pagination

		// Get page number from path `dashboard/:page`
		pageNumber := c.Param("page")
		page, err := strconv.Atoi(pageNumber)
		if err != nil || page < 1 {
			page = 1
		}

		// set pageSize to 8 for now
		pageSize := 8

		offset := (page - 1) * pageSize

		// get the rows paginated by offset
		rows, err := database.WrapQuery(dbconnections.MDMDBPool, ctx, "GetFromActivitiesPaginatedByOffset", pageSize, offset)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
		}
		defer rows.Close()

		results := make([]models.DashBoard, 0)

		for rows.Next() {
			var data models.DashBoard
			err := rows.Scan(&data.Sid, &data.Action, &data.By_who, &data.Table_name, &data.Created_at, &data.Updated_at)
			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
			}
			results = append(results, data)
		}

		// Get the total item of activities
		var totalItems int
		err = database.WrapQueryRow(dbconnections.MDMDBPool, ctx, "GetTotalSizeFromActivities").Scan(&totalItems)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQueryRow error")
		}

		totalPages := totalItems / pageSize
		nextPage := page + 1
		prevPage := page - 1

		templateDataMap["DataMap"] = DataMap
		templateDataMap["DashBoard"] = results
		templateDataMap["Page"] = page
		templateDataMap["PageSize"] = pageSize
		templateDataMap["TotalItems"] = totalItems
		templateDataMap["PageItemCount"] = pageSize
		templateDataMap["TotalPages"] = totalPages
		templateDataMap["NextPage"] = nextPage
		templateDataMap["PrevPage"] = prevPage

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "dashboard", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}
