#!/usr/bin/env bash
go clean all
swag init -g /cmd/main.go
go build -race -o ./bin/server_mac ./cmd
