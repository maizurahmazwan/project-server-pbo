package ui

import (
	"embed"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// embedded WebContent holds our static web server content.
// such as css, html, image, js files and HTML templates
// source taken from https://gist.github.com/wolfeidau/9f43e3f44cdb7a5f77405d3aa60421fc

//go:embed all:static all:templates
var WebContent embed.FS
var ContentHandler = echo.WrapHandler(http.FileServer(http.FS(WebContent)))
var ContentRewrite = middleware.Rewrite(map[string]string{"static/*": "/static/$1"})
