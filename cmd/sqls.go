package main

import (
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/logs"
)

const (
	Q_GET_ALL_FROM_ITEM_TYPES                     = `SELECT * FROM ITEM_TYPES ORDER BY SID`
	Q_GET_FROM_ITEM_TYPES_BY_SID                  = `SELECT * FROM ITEM_TYPES WHERE SID = $1`
	Q_UPDATE_ITEM_TYPES_BY_SID                    = `UPDATE ITEM_TYPES SET name = $1, value = $2 WHERE SID = $3`
	Q_INSERT_INTO_ITEM_TYPES                      = `INSERT INTO ITEM_TYPES(name,value) VALUES ($1::text,$2)`
	Q_DELETE_FROM_ITEM_TYPES                      = `DELETE FROM ITEM_TYPES WHERE SID = $1`
	Q_GET_ALL_FROM_HSCODES                        = `SELECT * FROM HSCODES ORDER BY hscode`
	Q_DELETE_ALL_FROM_HSCODES                     = `DELETE FROM HSCODES`
	Q_GET_ALL_FROM_CITIES                         = `SELECT * FROM CITIES ORDER BY CAST(postcode_from AS int)`
	Q_DELETE_ALL_FROM_CITIES                      = `DELETE FROM CITIES`
	Q_GET_ALL_FROM_DIALING_CODES                  = `SELECT * FROM COUNTRIES_DIALING_CODES ORDER BY country`
	Q_DELETE_ALL_FROM_DIALING_CODES               = `DELETE FROM COUNTRIES_DIALING_CODES`
	Q_GET_ALL_FROM_PRODUCT_CONNOTE_MAPPING        = `SELECT * FROM PRODUCT_CONNOTE_MAPPING ORDER BY prefix`
	Q_DELETE_ALL_FROM_PRODUCT_CONNOTE_MAPPING     = `DELETE FROM PRODUCT_CONNOTE_MAPPING`
	Q_INSERT_INTO_ACTIVITIES                      = `INSERT INTO ACTIVITIES(action,by_who,table_name) VALUES ($1::text,$2::text,$3::text)`
	Q_GET_PAGINATION_FROM_PRODUCT_CONNOTE_MAPPING = `SELECT * FROM PRODUCT_CONNOTE_MAPPING LIMIT $1 OFFSET $2`
	Q_GET_FROM_PRODUCT_CONNOTE_MAPPING_TOTAL_SIZE = `SELECT count(*) FROM PRODUCT_CONNOTE_MAPPING`
	Q_GET_PAGINATION_FROM_ACTIVITIES              = `SELECT sid, "action", by_who, table_name, created_at, updated_at
	FROM public.activities
	order by created_at desc  
	limit $1
	offset $2`
	Q_GET_FROM_ACTIVITIES_TOTAL_SIZE = `SELECT count(*) FROM ACTIVITIES`

	// https://gist.github.com/crgimenes/dc7723bf1d156fd0a29bce444a665e43
	Q_GET_AXIS_TRANSFORMATION_EVENTS = `SELECT id,message_id,headers,payload,event_code,created_date,http_status_code
	FROM public.T_AXIS_TRANSFORMATION_EVENTS
	ORDER BY id
	LIMIT $2
	OFFSET $1`

	Q_GET_AXIS_TRANSFORMATION_EVENTS_SIZE = `SELECT count(*) FROM public.T_AXIS_TRANSFORMATION_EVENTS`
)

var sqlStatements = map[string]string{
	// SQL Name : SQL statement
	// ok to have duplicate SQL statements, but not ok to have duplicate SQL names
	// Using map ensure that no duplicate keys. The compiler will stop if there's any duplicate in map literal

	"GetAllFromItemTypes":                           Q_GET_ALL_FROM_ITEM_TYPES,
	"GetFromItemTypesBySID":                         Q_GET_FROM_ITEM_TYPES_BY_SID,
	"UpdateItemTypesBySID":                          Q_UPDATE_ITEM_TYPES_BY_SID,
	"InsertIntoItemTypes":                           Q_INSERT_INTO_ITEM_TYPES,
	"DeleteFromItemTypes":                           Q_DELETE_FROM_ITEM_TYPES,
	"GetAllFromHSCodes":                             Q_GET_ALL_FROM_HSCODES,
	"DeleteAllFromHSCodes":                          Q_DELETE_ALL_FROM_HSCODES,
	"GetAllFromCities":                              Q_GET_ALL_FROM_CITIES,
	"DeleteAllFromCities":                           Q_DELETE_ALL_FROM_CITIES,
	"GetAllFromDialingCodes":                        Q_GET_ALL_FROM_DIALING_CODES,
	"DeleteAllFromDialingCodes":                     Q_DELETE_ALL_FROM_DIALING_CODES,
	"GetAllFromProductConnoteMapping":               Q_GET_ALL_FROM_PRODUCT_CONNOTE_MAPPING,
	"DeleteAllFromProductConnoteMapping":            Q_DELETE_ALL_FROM_PRODUCT_CONNOTE_MAPPING,
	"GetFromProductConnoteMappingPaginatedByOffset": Q_GET_PAGINATION_FROM_PRODUCT_CONNOTE_MAPPING,
	"GetTotalSizeFromProductConnoteMapping":         Q_GET_FROM_PRODUCT_CONNOTE_MAPPING_TOTAL_SIZE,
	"InsertIntoActivities":                          Q_INSERT_INTO_ACTIVITIES,
	"GetFromActivitiesPaginatedByOffset":            Q_GET_PAGINATION_FROM_ACTIVITIES,
	"GetTotalSizeFromActivities":                    Q_GET_FROM_ACTIVITIES_TOTAL_SIZE,

	"GetAxisTransformationEvents":     Q_GET_AXIS_TRANSFORMATION_EVENTS,
	"GetAxisTransformationEventsSize": Q_GET_AXIS_TRANSFORMATION_EVENTS_SIZE,
}

func init() {
	database.InitSQLStatements(sqlStatements)
	logs.Info().Msg("Initialized SQL statements")
}
