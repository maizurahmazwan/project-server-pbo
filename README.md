# BackOffice

**BackOffice** is a back office application that allows product owners to update or change backend services data. It uses PocketBase as backend to authenticate users. 

Authenticated users are allowed to upload new data for backend services such as Track and Trace. It also serves pages for updating backend data.

PocketBase will be used to provide backend services for **BackOffice**. User registration will be done at PocketBase's Admin UI. User data is stored inside PocketBase's SQLite. 

**BackOffice** uploads new data files to replace existing data files in S3. For example, for Track & Trace, **BackOffice** will overwrite the copywriting.csv file in TTU's S3 bucket.

# Contact 

Phua Kim Leng - phua.kim@pos.com.my

Ng Boo Jiun - boojiun@pos.com.my

# Links

Confluence link:
```
https://pos-malaysia.atlassian.net/l/cp/Vwq13xnS 
```

# Staging Server

Each time code is pushed to main branch, staging server will be updated with the latest build from git. An alert will be sent out in Teams under POS Digital DevOps > General > OPS_CICD channel

Staging server can be accessed through URL:
```
https://pbo.uat-pos.com/mdm
```

- Note: **Wait 5 minutes** for staging server update.
- Note: URL is without forward slash. For example, https://pbo.uat-pos.com/mdm/ will not work.

# Track & Trace AWS Configuration

The development machine needs to be configured accordingly before it can be used to test Track & Trace S3 uploading to bucket. 

SSO configuration for testing:
```
https://pos-malaysia.atlassian.net/wiki/spaces/PDP/pages/26021470/AWS+SSO+Refresh+auth+token
```

# Postgres Database

Local debugging requires connection to Postgres database. Connection to GlobalProtect VPN required before accessing Postgres database.
